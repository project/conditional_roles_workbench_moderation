<?php
/**
 * @file
 * Provides moderation links for Views.
 *
 * This file can be deleted if the proposed hook in issue 1108502 is accepted.
 *
 * @see http://drupal.org/node/1108502
 * @see workbench_moderation_handler_field_links.inc
 */

/**
 * Implements hook_views_data_alter().
 */
class conditional_roles_workbench_moderation_handler_field_links extends views_handler_field {
  function render($values) {
    if ($values->{$this->aliases['current']}) {
      $node = node_load($values->{$this->aliases['nid']}, $values->{$this->aliases['vid']});
      // Use our function to get the moderation links.
      return theme('links', array('links' => conditional_roles_workbench_moderation_get_moderation_links($node, array('query' => array('destination' => $_GET['q'])))));
    }
    return '';
  }
}
