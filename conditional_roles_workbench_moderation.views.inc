<?php
/**
 * @file
 * Overrides for Workbench Moderation's Views integration.
 *
 * This file can be deleted if the proposed hook in issue 1108502 is accepted.
 *
 * @see http://drupal.org/node/1108502
 */

/**
 * Implements hook_views_data_alter().
 */
function conditional_roles_workbench_moderation_views_data_alter(&$data) {
  $data['workbench_moderation_node_history']['moderation_actions']['field']['handler'] = 'conditional_roles_workbench_moderation_handler_field_links';
}
